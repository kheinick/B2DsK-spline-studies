#! /usr/bin/env python3

import pickle
import sys

import numpy as np
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
        "font.size": 12,
        "font.family": "serif",
        "font.serif": [],                   # use latex default serif font
        "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
        }
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt


pickle_filename = sys.argv[1]
fig = pickle.load(open(pickle_filename, 'rb'))
data = [l.get_data() for l in fig.axes[0].lines]

fig = plt.figure(figsize=(6, 4))
for i, ((x, y), (args, kwargs)) in enumerate(zip(data, [
        (['C0'], dict(label='Cubic B-Spline')),
        (['C0--'], dict(label='Extrapolation')),
        (['C0--'], {}),
        (['C1:'], dict(label='Internal quadratic B-spline')),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C1:'], {}),
        (['C2o'], dict(label='Internal knot position')),
        ])):
    if i in range(11):
        x = x[y>0]
        y = y[y>0]
    plt.plot(x, y, *args, **kwargs)

plt.ylim(-0.02, 1.12)
plt.xlim(0, 15)
plt.legend(loc='center right', bbox_to_anchor=(1, 0.62))
plt.xlabel(r'$\tau\,\left[\mathrm{ps}\right]$', horizontalalignment='right', x=1)
plt.ylabel(r'Arbitrary Units', horizontalalignment='right', y=1)
plt.tight_layout()

plt.savefig('plot.pdf')
plt.savefig('plot.pgf')
